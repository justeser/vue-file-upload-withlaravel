Route::post('upload', function(Request $request){
    $file = request()->file('image');
    $destinationPath = base_path().'/public/images/';
    $ext  = $file->getClientOriginalExtension();
    $fileName = rand(11111,99999).'.'.$ext;
    $file->move($destinationPath, $fileName);
    return response($request->id.' uploaded', 200);

});
